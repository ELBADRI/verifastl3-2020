# Verifast, une introduction

Verifast est un logiciel servant à prouver la correction de programmes écrits en C (et Java).

Il est relativement facile à utiliser, sans configuration à faire et surtout il existe des binaires prêts à être éxécutés sur toutes les plateformes. Cependant, c'est un logiciel en cours de développement, pas forcément facile à prendre en main. 

## Préparation

Le logiciel verifast est disponible sur [github](https://github.com/verifast/verifast), un lien vers les binaires est disponible sur le site.

Dans le dossier **bin**, l'executable vfide lancera l'éditeur graphique. *Important :* dans le menu décochez Verify>Check arithmetic overflow.

Finalement, créez un répertoire pour cette scéance, téléchargez-y le fichier [lemmas](lemmas.gh).

## On y va

### Les bases

#### Squelette

Créez un fichier `test.c` (ou soyez original dans le nom). 
Le squelette minimal s'apparente à 
```c
#include "stdlib.h"

int main()
{
    return 0;
}
```

Appuyez sur le bouton lecture ou menu Verify>Verify programm. Le programme se plaint «Contract required».

Un contract représente une assertion en entrée (requires) et une assertion en sortie (ensures) d'une fonction, comme pour la logique de Hoare.

Rajoutez entre le `main()` et la première accolade 

```c
//@ requires true;
//@ ensures true;
```

Tout ce qui sert à vérifier son programme est écrit sous forme de commentaire, mais avec une accolade. Ainsi le fichier peut compiler avec gcc par exemple. 

Ici le contrat est simple on ne suppose rien en entrée et en sortie. On suppose juste vrai. La syntaxe des expressions booléennes est la même qu'en C à détail près que l'on verra plus loin.

*Pourquoi suppose-t-on vrai, et pas faux ?*

<details>
<summary>Réponse</summary>
Pour l'assertion A implique B, si A est faux, l'assertion est toujours vraie. Si l'on supposait faux en entrée alors, l'assertion de sortie serait toujours validée (même si elle n'est pas vraie).
</details>

#### Un exemple plus intéressant : maximum

Ajoutez la fonction 
```c
int myMax(int a, int b)
{
	return a>b ? a : b;
}
```

Notez que Verifast ne se plaint pas du manque de contrat. Tout simplement `myMax` n'est pas appellée. Complétons la fonction `main` :
```c
int main()
//@ requires true;
//@ ensures true;
{
	int c=myMax(10, 2);
	assert( c == 10);
	return 0;
}
```

**Exercice** maximum

Ajoutez un contrat sur la fonction `myMax`. On ne supposera rien de particulier. Pour le ensure, on utilise plusieurs propriétés (A et B et C). Au lieu du `&&`, verifast utilise l'opérateur `&*&`. On a besoin de faire référence au résultat renvoyé. Pour cela on utilise la variable `result`. On écrira par exemple `//@ ensures result>=0 &*& ...` Essayez de trouver une assertion pour le requires.

**Important** Lorsque Verifast indique une erreur, il la souligne en rouge. En accompagnement il y a une liste "Assumptions" qui indique les propriétés connues quand il y a une erreur, il se pourrait que vous y voyez un manque de propriétés que vous pouvez ajouter dans le requires.

<details>
<summary>Indice 1</summary>
Le résultat est plus grand que a et que b, non ?
</details>

<details>
<summary>Indice 2</summary>
Le résultat est soit a, soit b.
</details>


<details>
<summary>Réponse</summary>
```c
int myMax(int a, int b)
//@ requires true;
//@ ensures result>=a &*& result>=b &*& result==a||result==b;
{
	return a>b ? a : b;
}
```
</details>

**Exercice** minimum

De même, ajoutez une fonction minimum et vérifiez-là.

### La banque 

Maintenant, nous allons créer des fonctions plus intéressantes, portant sur des comptes en banques.
Pour un fichier c propre, il faudrait créer une structure de données adéquate, ce que l'on ne fera pas ici, pour une raison de simplicité. Référez-vous au tutoriel si vous voulez creuser dans cette direction.

**Exemple** Dépot d'argent

```c
void charger_compte(int* solde, int apport)
//@ requires integer(solde, ?s) &*& apport>0;
//@ ensures integer(solde, s+apport);
{
	(*solde)+=apport;
}
```

Cette fonction introduit 3 nouveautés. Tout d'abord le require n'est pas juste vrai. En paticulier la somme déposée doit être positive (je vous vois petits malins qui essayez de hacker le système). Ensuite le ?s permet de parler d'une valeur en entrée, le ? définit cette variable. Ici on parle du solde du compte initial s, et on vérifie que le nouveau solde est la somme de l'ancien solde avec le nouveau. Finalement, la fonction integer(x,y) permet de déréférencer x : la valeur pointée par x est y. Ici la valeur du solde nous intéresse.

Testez `//@ ensures true;`. Vous obtenez le message d'erreur "Function leaks heap chunks." 
Verifast détecte alors une fuite mémoire, il s'attend en effet à ce que solde soit détruit. Si on ne veut pas spécifier la valeur du nouveau solde, il faut au moins avoir `//@ ensures integer(solde, _);`. Le _ indique une valeur quelconque.

**Exercice** Retrait d'argent

Créer une fonction
```c
int debit(int* solde, int limit, int retrait);
```
Cette fonction est appellée si un utilisateur veut retirer `retrait`. Le solde ne doit jamais être inférieur à `limit`. La fonction renvoie le montant débité, qui est égal à `retrait` si les fonds sont suffisant, maximum dans le cas échéant. Utilisez l'opérateur ternaire `condition?a:b` dans les prédicats pour dire a si la condition est vraie, b sinon.

<details>
<summary>Ne regardez pas la solution avant d'avoir essayé d'ajouter un contrat</summary>
```c
int debit(int* solde, int limit, int retrait)
{
	int r=(*solde-retrait<=limit)? *solde-limit: retrait;
	*solde-=r;
	return r;
}
```
</details>

Augmentez le main avec quelques tests. 
```c
int main()
//@ requires true;
//@ ensures true;
{
	int c=myMax(10, 2);
	int solde =42;
	assert( c == 10);
	int d = debit(&solde, -10, 30);
	assert (solde == 12);
	charger_compte(&solde, 20);
	assert (solde == 32);
	d = debit(&solde, -10, 50);
	assert (solde == -10);
	assert (d == 42);
	return 0;
}
```

Ensuite ajoutez un contrat qui doit vérifier que tout concorde avec l'énoncé (si verifast passe sans problème sur les assertions votre contrat est suffisant). En cas de difficulté regardez la solution ci-dessus puis réessayez.

<details>
<summary>Solution (contrat uniquement)</summary>
```c
//@ requires integer(solde, ?s) &*& retrait>0 &*& s>=limit;
//@ ensures integer(solde, s-result) &*& result<=retrait &*& s-result>=limit &*& result == ((s-retrait<=limit) ? s-limit:retrait);
```
</details>

## Boucles

Cette dernière partie est la plus importante. Jusqu'ici les fonctions étaient simple. Maintenant, on ajoute une boucle. 

### Commencons simple

Les boucles ont besoin d'un invariant et d'un variant. Nous avons vu en TD qu'un invariant est une assertion. Le variant est une expression arithmétique décroissante.
Avec verifast on écrit (on peut toujours utiliser les conjonctions `A&*&B`)

 ```c 
 while(a0>0)
  //@ invariant mon_assertion;
  //@ decreases expr;
  ```

Le premier exercice utilise la fonction ci dessous :

```c
int add(int i0, int j0)
//@ requires j0>=0;
//@ ensures result == i0+j0;
{
	int i=i0;
	int j=j0;
	while(j>0)
	//@ invariant …;
	//@ decreases …;
	{
		i++;
		j--;
	}
	assert (j==0);
	return i;
}
```

Complétez les trous 

<details>
<summary>Solution</summary>
```c
int add(int i0, int j0)
//@ requires j0>=0;
//@ ensures result == i0+j0;
{
	int i=i0;
	int j=j0;
	while(j>0)
	//@ invariant i+j==i0+j0 &*& j>=0;
	//@ decreases j;
	{
		i++;
		j--;
	}
	assert (j==0);
	return i;
}
```
</details>

###  Multiplication rapide

Reprenez la multiplication rapide du TD 0 (ci dessous).
![alt text1](Capture.JPG)

Implémetez la version en C dans un nouveau fichier

<details>
<summary>Solution</summary>
```c
int mul(int a, int b)
// requires a>=0;
// ensures result==a*b;
{
  int r=0;
  int a0=a;
  int b0=b;
  while(a0>0)
  {
    if(a0%2==1) {
      r=r+b0;
    }
    else {
    }
    a0/=2;
    b0=2*b0;
  }
  assert(a0==0);
  return r;
}
```
</details>

Ensuite ajoutez en début de fichier la ligne `//@ #include "lemmas.gh"`

Le contrat que nous utilisons ici est 
```c
//@requires a>=0 &*& b>=0;
//@ensures result==a*b;
```

Invariant et variant ne suffisent pas, il faut aider verifast. Il existe des lemmes que l'on peut appeler dans le code, cela permet de rajouter de étapes intermédiaires. C'est comme si on appelait une fonction avec un contrat. Cela permet de transformer les assertions

On appele un lemme par `//@ monlemme(var1, var2);`

On liste les lemmes utiles :

```c
lemma void mul_div_2_r_1(int a, int b)
	requires a%2==1 &*& a>=0;
	ensures (a/2)*(2*b)+b==a*b;
```
```c
lemma void mod_excl_2(int a)
	requires !(a%2==1) &*& a>=0;
	ensures a%2==0;
```
```c
lemma void mul_div_2(int a, int b)
	requires a%2==0 &*& a>=0;
	ensures (a/2)*(2*b)==a*b;
```
```c
lemma void div_rem_nonneg(int D, int d);
    requires 0 <= D &*& 0 < d;
    ensures D == D / d * d + D % d &*& 0 <= D / d &*& D / d <= D &*& 0 <= D % d &*& D % d < d;
```
<details>
<summary>Solution</summary>
```c
//@ #include "lemmas.gh"

int mul(int a, int b)
//@requires a>=0 &*& b>=0;
//@ensures result==a*b;
{
  int r=0;
  int a0=a;
  int b0=b;
  while(a0>0)
  //@ invariant a*b == r+a0*b0 &*&a0>=0;
  //@ decreases a0;
  {
    if(a0%2==1) {
      //@ mul_div_2_r_1(a0, b0);
      r=r+b0;
    }
    else {
      //@ mod_excl_2(a0);
      //@ mul_div_2(a0, b0);
    }
    //@div_rem_nonneg(a0,2);
    a0/=2;
    b0=2*b0;
  }
  assert(a0==0);
  return r;
}
```
</details>
